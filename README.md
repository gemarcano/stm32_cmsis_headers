This is a combination of relevant headers from
git@github.com:ARM-software/CMSIS_5.git and
git@github.com:STMicroelectronics/cmsis_device_l0.git.

All files are covered under the Apache2 license, with a copy included in
LICENSE.txt.


# Building

Add custom constants to `my_cross.txt`, e.g. overriding the prefix where
`sys_root` is located.

```
meson --prefix ${PREFIX} ../ --cross-file ../cross_file.txt --cross-file ../my_cross.txt
```

# Installing
```
meson install
```
